﻿using System;
using System.Threading.Tasks;

namespace RazorFin_fix
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Application.Run();
        }
    }
}
