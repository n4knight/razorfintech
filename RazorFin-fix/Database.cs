﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RazorFin_fix
{
    class Database
    {
        public Dictionary<string, List<AccountDetails>> _accountDetails;
        public Dictionary<string, List<Credentials>> _credentials;

        public Database()
        {
            _accountDetails = new Dictionary<string, List<AccountDetails>> { {"user1Email@chiii.com".ToLower(),
                    new List<AccountDetails> { new AccountDetails { Fullname = "User1", Address="user1Address", Email ="user1Email@chiii.com",
                    Password="User1Password", Phonenumber="user1Phonenumber"} } } };

            _credentials = new Dictionary<string, List<Credentials>> { { "user1Email@chiii.com".ToLower(), new List<Credentials> {
                new Credentials{Email="user1Email".ToLower(), password="User1password".ToLower()}} } };
        }

        public async Task Login(Credentials credential)
        {


            if (!_credentials.ContainsKey(credential.Email))
            {
                Console.WriteLine($"No record with {credential.Email} found");
                return;
            }

            Console.WriteLine($"Hello {await GetAccountName(credential.Email)}, check Below for your details.");
            AccountDetails _searchAccount =  GetAccount(credential.Email);
            Console.WriteLine("Getting your details...");

            await Task.Delay(3000);

            Console.WriteLine();
            if (_searchAccount is null)
            {
                Console.WriteLine("Nothing was returned");
                return;
            }

            Console.WriteLine($"Your name { _searchAccount.Fullname}");
            Console.WriteLine($"Your Email { _searchAccount.Email}");
            Console.WriteLine($"Your Address { _searchAccount.Address}");
            Console.WriteLine($"Your Password { _searchAccount.Password}");
            Console.WriteLine($"Your Phonenumber { _searchAccount.Phonenumber}");

            Console.WriteLine();
        }

        public async Task<string> GetAccountName(string input)
        {
            Console.WriteLine("Checking Database...");

            await Task.Delay(1000);
            string name = "";
            var _accountQuery = _accountDetails[input].Where(account => account.Email == input);
            foreach (AccountDetails acc in _accountQuery)
            {
                name = acc.Fullname;
            }

            return name;
        }


        public  AccountDetails GetAccount(string searchQuery)
        {
            AccountDetails _account;

            if (_accountDetails.ContainsKey(searchQuery))
            {
                return _account = _accountDetails[searchQuery][0];
            }

            return null;
        }

    }  
}
