﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFin_fix
{
	class Application
	{
		private static readonly Database _dbinstance = new Database();

		public async static Task Run ()
        {

            //Use => user1Email@chiii.com 4 email and user1Password 4 password

			MainMenu:
				Console.WriteLine("Log in to see your details"); 

				Console.WriteLine("enter email");
				var email = Console.ReadLine().ToLower();

				Console.WriteLine("enter password");
				var password = Console.ReadLine().ToLower();

			while(string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
            {
				goto MainMenu;
            }

            Console.WriteLine();

			Credentials c = new Credentials { Email = email, password = password };
			await _dbinstance.Login(c);
        }
	}
}
