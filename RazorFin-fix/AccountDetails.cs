﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFin_fix
{
    public class AccountDetails
    {
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phonenumber { get; set; }
        public string Address { get; set; }
    }

    public class Credentials
    {
        public string Email { get; set; }
        public string password { get; set; }
    }
}
